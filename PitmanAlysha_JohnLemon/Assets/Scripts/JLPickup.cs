﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JLPickup : MonoBehaviour
{
    public GameObject[] turnoff; //disable game object
    private void OnTriggerEnter(Collider other) //when a trigger event happens between this & another object
    {
        if (other.gameObject.CompareTag("Player")) //if the other object is tagged "player"
        {
            for (int i = 0; i < turnoff.Length; i++) //loop through each index of the 'turnoff' array
            {
                turnoff[i].SetActive(false); //disable what is listed in the 'turnoff' array
            }
            gameObject.SetActive(false); //deactivate that object
        }
    }
}