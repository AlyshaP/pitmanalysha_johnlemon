﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BigGhostAppear : MonoBehaviour
{
    public GameObject BigGhost; //Drag and drop in reference to the BigGhost instance in the scene (not the prefab anymore)
    void OnTriggerEnter()
    {
        BigGhost.SetActive(true); //Turn on the ghost
    }
}